﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelFilesSpecified
{
    class Program
    {
        static void Main(string[] args)
        {
            string folderPath = "";
            string pattern = "";

            Console.WriteLine("Введите путь к папке:");
            folderPath = Console.ReadLine();

            ShowFiles(folderPath);

            Console.WriteLine("\nВведите часть имени файлов, которые надо удалить:");
            pattern = Console.ReadLine();

            DeleteFilesLike(folderPath, pattern);

            ShowFiles(folderPath);

            Console.WriteLine("\nДля завершения нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void DeleteFilesLike(string folderPath, string pattern)
        {
            string[] paths = Directory.GetFiles(folderPath);
            foreach (string file in paths)
            {
                if (Path.GetFileName(file).Contains(pattern))
                {
                    File.Delete(file);
                }
            }
        }

        private static void ShowFiles(string path)
        {
            string[] paths = Directory.GetFiles(path);

            int i = 0;
            Console.WriteLine("\nФайлы в папке:");
            foreach (string item in paths)
            {
                i++;
                Console.WriteLine($"{i}. {Path.GetFileName(item)}");
            }
        }
    }
}
